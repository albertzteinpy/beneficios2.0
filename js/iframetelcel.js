

		var _layout_anonymous_shift_pressed = false;
		var KEYCODE_SHIFT = 16;

		(function($) 
		{  
		    $.fn.filterNumbersOnly = function(options) 
		    {  
		        this.each(function() 
		        {
		            var element = $(this);
		            $(element).keydown(isNumeric);
		            $(element).keypress(isNumeric);
		            $(element).keyup(isShiftReleased);
		        });
		    }
		})(jQuery);
		

		function isShiftReleased(e)
		{
			if (_layout_anonymous_shift_pressed)
			{
			    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
			    var code = parseInt(key, 10);
			    
			    if (code == KEYCODE_SHIFT)
			    {
			    	_layout_anonymous_shift_pressed = false;
			    }
			}
		}


		function isNumeric(e) {
		    return e.charCode === 0 || /\d/.test(String.fromCharCode(e.charCode));
		};

		function isAlpha(e) {
			
			e = e || window.event;
		    var keyCode = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		    
		    if( keyCode == 225  || keyCode == 233 || keyCode == 237 || keyCode == 243 || keyCode == 250 || keyCode == 39)
		    {
		        return true;
		    }
		    else
		    {
		        var charCode = (typeof event.which == "undefined") ? event.keyCode : event.which;
		        return charCode === 0 || charCode === 8 || /^[a-zA-ZñÑ\s]*$/.test(String.fromCharCode(charCode));
		    }
		     
		}

		setInterval(function() {
			var height = $(".container-iframe").height() + 100;
			parent.postMessage(height,"*");
		},100);